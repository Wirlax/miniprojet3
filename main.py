import argparse
import datetime
import sqlite3
import logging
import sys
import os
import csv
from os import path

logging.basicConfig(format='%(levelname)s:%(asctime)s:%(message)s', filename='logbdd.log', level=logging.DEBUG)


def parseArgs():
    parser = argparse.ArgumentParser(description='Add vehicles from csv file to a database')
    parser.add_argument('--delim', required=False, default='|', type=str, help='delimiter for csv file')
    parser.add_argument('--database', required=True, type=str, help='data base file')
    parser.add_argument('--csv', required=True, type=str, help='csv file to parse and stock')
    return parser.parse_args()


def CreateTable(file):
    open("./" + file, 'a').close()
    bddvoiture = sqlite3.connect(file)
    cursorvoiture = bddvoiture.cursor()
    try:
        cursorvoiture.execute("CREATE TABLE SIV (\
        adresse_titulaire TEXT, \
        nom TEXT, \
        prenom TEXT, \
        immatriculation TEXT UNIQUE, \
        date_immatriculation TEXT, \
        vin TEXT, \
        marque TEXT, \
        denomination_commerciale TEXT, \
        couleur TEXT, \
        carrosserie TEXT, \
        categorie TEXT, \
        cylindre TEXT, \
        energie TEXT, \
        places TEXT, \
        poids TEXT, \
        puissance TEXT, \
        type TEXT, \
        variante TEXT, \
        version TEXT)")
        logging.info("La table à etee Creee")
        bddvoiture.commit()
    except:
        logging.info("La table existe deja")
    return bddvoiture


def Reformat(line):
    nouvelordre = [0, 9, 10, 8, 11, 4, 7, 12, 2, 3, 6, 1, 13, 14, 15, 16, 5]
    nouvelleline = [''] * 19
    for idx, param in enumerate(line):
        if idx == 15:
            newparam = param.split(', ')
            for count, info in enumerate(newparam):
                nouvelleline[nouvelordre[idx] + count] = info
        elif idx == 5:
            try:
                newdate = datetime.datetime.strptime(param, '%Y-%m-%d')
                nouvelleline[nouvelordre[idx]] = newdate.strftime('%d/%m/%Y')
            except:
                nouvelleline[nouvelordre[idx]] = param
        else:
            nouvelleline[nouvelordre[idx]] = param
    return nouvelleline


def addVehicle(line, connection):
    c = connection.cursor()
    # SQLite command to add or replace a vehicle in the database
    sendVehicleCmd = "INSERT OR REPLACE INTO SIV VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
    # SQLite command to check if the vehicle already exist
    vehicleExistCmd = "SELECT * FROM SIV WHERE immatriculation=?"
    # Check if the vehicle exist and create or replace him
    c.execute(vehicleExistCmd, [line[3]])
    if not c.fetchone():
        logging.info("Sending vehicle " + line[3] + " to data base")
    else:
        logging.info("Vehicle " + line[3] + " already exist, updating him")
    c.execute(sendVehicleCmd, line)
    connection.commit()


def sendSplittedFileToDb(file, delim, connection):
    # Check if the file exist
    if not os.path.exists(file):
        logging("Csv File doesn't exist or is unreadable")
        exit(1)
    #logging("Start reading " + file + " file")
    with open(file, 'r') as csvfile:
        # Protect file closing by using atexit
        reader = csv.reader(csvfile, delimiter=delim)
        firstline = True
        nbLine = 1
        for line in reader:
            # Don't read the first line of the file
            if firstline == True:
                firstline = False
            else:
                newLine = Reformat(line)
                addVehicle(newLine, connection)
            nbLine += 1
    csvfile.close()


def main():
    args = parseArgs()
    connection = CreateTable(args.database)
    sendSplittedFileToDb(args.csv, args.delim, connection)
    connection.close()


main()
